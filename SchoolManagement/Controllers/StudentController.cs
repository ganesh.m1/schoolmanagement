
using SCHOOL_DAL.Models; 
using SCHOOL_DAL.Interface;  
using SCHOOL_BAL.Service;
using Microsoft.AspNetCore.Http;  
using Microsoft.AspNetCore.Mvc;  
using Newtonsoft.Json;  
using System;  
using System.Collections.Generic;  
using System.Linq;  
using System.Threading.Tasks; 
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.JsonPatch;

  
namespace coreWebAPI_CF.Controllers  
{  
    [Route("api/[controller]")]  
    [ApiController]  
    public class StudentController : ControllerBase  
    {  
        private readonly StudentService _StudentService;  
  
        private readonly IStudentRepository<Student> _Student;  
  
        public StudentController(IStudentRepository<Student> Student, StudentService StudentService)  
        {  
            _StudentService = StudentService;  
            _Student = Student;  
  
        }  
        //Add Person  
        [HttpPost("AddStudent")]  
        public async Task<Object> AddStudent([FromBody] Student Student)  
        {  
            try  
            {  
                await _StudentService.AddStudent(Student);  
                return true;  
            }  
            catch (Exception)  
            {  
  
                return false;  
            }  
        }
        //Delete Person  
        [HttpDelete("DeleteStudent")]  
        public bool DeleteStudent(int Id)  
        {  
           try  
            {  
                var DataList = _Student.GetAll().Where(x => x.Id == Id).ToList();  
                foreach (var item in DataList)  
                {  
                    _Student.Delete(item);  
                }  
                return true;  
            }  
            catch (Exception)  
            {  
                return true;  
            }  
        }  

        //Delete Person  
        [HttpPut("UpdateStudent")]  
        public bool UpdateStudent(Student Object)  
        {  
            try  
            {  
                _StudentService.UpdateStudent(Object);
                return true;  
            }  
            catch (Exception)  
            {  
                return false;  
            }  
        }  
        
        //GET All Person by Name  
        [HttpGet("GetStudentByName")]  
        public Object GetStudentByStudentName(string Name)  
        {  
            var data = _StudentService.GetStudentByStudentName(Name);  
            var json = JsonConvert.SerializeObject(data, Formatting.Indented,  
                new JsonSerializerSettings()  
                {  
                    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore  
                }  
            );  
            return json;  
        }  
         //GET Student by Id  
        [HttpGet("GetStudentById")]  
        public Object GetStudentById(int Id)  
        {  
            var data = _StudentService.GetStudentById(Id);  
            var json = JsonConvert.SerializeObject(data, Formatting.Indented,  
                new JsonSerializerSettings()  
                {  
                    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore  
                }  
            );  
            return json;  
        }  
  
        //GET All Person  
        [HttpGet("GetAllStudents")]  
        public Object GetAllStudents()  
        {  
            var data = _StudentService.GetAllStudents();  
            var json = JsonConvert.SerializeObject(data, Formatting.Indented,  
                new JsonSerializerSettings()  
                {  
                    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore  
                }  
            );  
            return json;  
        }  

        
        [HttpPatch("{id}")]
        public async Task<IActionResult> UpdateStudentPatch([FromBody] JsonPatchDocument Student, [FromRoute] int id)
        {
            await _StudentService.UpdateStudentPatch(id, Student);
            return Ok();
        }   
    }  
}