using SCHOOL_DAL.Interface;  
using SCHOOL_DAL.Models;  
using System;  
using System.Collections.Generic;  
using System.Linq;  
using System.Text;  
using System.Threading.Tasks;  
using Microsoft.AspNetCore.JsonPatch;

namespace SCHOOL_BAL.Service  
{  
    public class StudentService  
    {  
        private readonly IStudentRepository<Student> _Student;  
  
        public StudentService(IStudentRepository<Student> Student)  
        {  
            _Student = Student;  
        }  
        //Get Person Details By Person Id  
        public IEnumerable<Student> GetStudentById(int Id)  
        {  
            return _Student.GetAll().Where(x => x.Id == Id).ToList();  
        }  
        //GET All Perso Details   
        public IEnumerable<Student> GetAllStudents()  
        {  
            try  
            {  
                return _Student.GetAll().ToList();  
            }  
            catch (Exception)  
            {  
                throw;  
            }  
        }  
        //Get Person by Person Name  
        public Student GetStudentByStudentName(string Name)  
        {  
            return _Student.GetAll().Where(x => x.StudentName == Name).FirstOrDefault();  
        }  
        //Add Person  
        public async Task<Student> AddStudent(Student Student)  
         {  
             Student objStudent = new Student();
             objStudent.Id=Student.Id;
            objStudent.CreatedBy = 121;
            objStudent.CreatedOn = DateTime.Now;
            objStudent.ModifiedBy = 121;
            objStudent.ModifiedOn = DateTime.Now;
            return await _Student.Create(objStudent);
        } 


        //Delete Person   
        public bool DeleteStudent(string Email)  
        {  
  
            try  
            {  
                var DataList = _Student.GetAll().Where(x => x.Email == Email).ToList();  
                foreach (var item in DataList)  
                {  
                    _Student.Delete(item);  
                }  
                return true;  
            }  
            catch (Exception)  
            {  
                return true;  
            }  
  
        }  
        //Update Person Details  
        public bool UpdateStudent(Student Student)  
        {  
            try  
            {  
                 _Student.Update(Student);
                
                return true;  
            }  
            catch (Exception)  
            {  
                return false;  
            }  
        }  
        //Using PATCH for partial update
        public async Task UpdateStudentPatch(int id, JsonPatchDocument Student)
        {
            try
            {
                //_store.Update(store);
                await _Student.UpdateStudentPatchAsync(id, Student);



            }
            catch (Exception)
            {

            }
        }
    }  
}  