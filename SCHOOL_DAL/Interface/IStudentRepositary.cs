using System;  
using System.Collections.Generic;  
using System.Text;  
using System.Threading.Tasks;  
using Microsoft.AspNetCore.JsonPatch;
  
namespace SCHOOL_DAL.Interface  
{  
    public interface IStudentRepository<T>  
    {  
        public Task<T> Create(T _object);  
  
        public void Update(T _object);  
         Task UpdateStudentPatchAsync(int Id, JsonPatchDocument Student);
  
        public IEnumerable<T> GetAll();  
  
        public T GetById(int Id);  
  
        public void Delete(T _object);  
  
    }  
}  