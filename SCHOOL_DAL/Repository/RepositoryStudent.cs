
using SCHOOL_DAL.Data;
using SCHOOL_DAL.Interface;
using SCHOOL_DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch;


namespace SCHOOL_DAL.Repository
{
    public class RepositoryStudent : IStudentRepository<Student>
    {
        ApplicationDbContext _dbContext;
        public RepositoryStudent(ApplicationDbContext applicationDbContext)
        {
            _dbContext = applicationDbContext;
        }
        public async Task<Student> Create(Student _object)
        {
            var obj = await _dbContext.Students.AddAsync(_object);
            _dbContext.SaveChanges();
            return obj.Entity;
        }

        public void Delete(Student _object)
        {
            _dbContext.Remove(_object);
            _dbContext.SaveChanges();
        }

        public IEnumerable<Student> GetAll()
        {
            try
            {
                return _dbContext.Students.Where(x => x.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
    
        public Student GetById(int Id)
        {
            return _dbContext.Students.Where(x => x.IsDeleted == false && x.Id == Id).FirstOrDefault();
        }

        public void Update(Student _object)
        {
            _dbContext.Students.Update(_object);
            _dbContext.SaveChanges();
        }

        public async Task UpdateStudentPatchAsync(int Id, JsonPatchDocument Student)
        {
            var Students = await _dbContext.Students.FindAsync(Id);
            if (Student != null)

            {
                Student.ApplyTo(Students);
                await _dbContext.SaveChangesAsync();
            }
        }
    }
}