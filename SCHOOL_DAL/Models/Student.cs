using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace SCHOOL_DAL.Models
{
    [Table("Student", Schema = "dbo")]
    public class Student
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Column(TypeName = "VARCHAR")]
        [StringLength(100)]
        [Display(Name = "Name")]  
        public string StudentName { get; set; }  


        [Required]
        [Column(TypeName = "VARCHAR")]
        [StringLength(12)]
        [Display(Name = "RegisterNo")]  
        public int RegisterNo { get; set; } 


        [Required]
        [Display(Name = "Class")]  
        public String Class { get; set; } 


        [Required]  
        [Display(Name = "StudentAge")]  
        public int StudentAge { get; set; }

         [Required]
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        [Display(Name = "FatherName")]  
        public string FatherName { get; set; } 

        [Required]
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        [Display(Name = "MotherName")]  
        public string MotherName { get; set; } 


        [Required]  
         [Column(TypeName = "VARCHAR")]
        [StringLength(10)]
        [Display(Name = "DOB")]  
        public int DOB { get; set; }

        [Required]
        [Column(TypeName = "VARCHAR")]
        [StringLength(100)]
        [Display(Name = "Email")]  
        public string Email { get; set; } 



        [Required]  
        [Display(Name = "Gender")]  
        public string Gender { get; set; }


        [Required]  
        [Display(Name = "BloodGroup")]  
        public string BloodGroup { get; set; }


        [Required] 
        [Column(TypeName = "VARCHAR")]
        [StringLength(12)] 
        [Display(Name = "MobileNo")]  
        public int MobileNo { get; set; }


        [Required] 
        [Column(TypeName = "VARCHAR")]
        [StringLength(100)] 
        [Display(Name = "FatherOccupation")]  
        public string FatherOccupation { get; set; }

        [Required] 
        [Column(TypeName = "VARCHAR")]
        [StringLength(100)] 
        [Display(Name = "MotherOccupation")]  
        public string MotherOccupation { get; set; }

        [Required] 
        [Column(TypeName = "VARCHAR")]
        [StringLength(10)] 
        [Display(Name = "AnnualIncome")]  
        public int AnnualIncome { get; set; }

        [Required] 
        [Column(TypeName = "VARCHAR")]
        [StringLength(100)] 
        [Display(Name = "Address")]  
        public string Address { get; set; }

        [Required] 
        [Column(TypeName = "VARCHAR")]
        [StringLength(20)] 
        [Display(Name = "Nationality")]  
        public string Nationality { get; set; }

        [Required] 
        [Column(TypeName = "VARCHAR")]
        [StringLength(20)] 
        [Display(Name = "AadhaarNo")]  
        public string AadhaarNo { get; set; }


        [Required]  
        [Display(Name = "Status")]  
        public bool Status { get; set; }

        [Required]  
        [Display(Name = "IsDeleted")]  
        public bool IsDeleted { get; set; } 

        [Required]  
        [Display(Name = "CreatedBy")]  
        public int CreatedBy { get; set; } 

        [Required]  
        [Display(Name = "ModifiedBy")]  
        public int ModifiedBy { get; set; } 
  
        [Required]  
        [Display(Name = "CreatedOn")]  
        public DateTime CreatedOn { get; set; } = DateTime.Now;  

        [Required]  
        [Display(Name = "ModifiedOn")]  
        public DateTime ModifiedOn { get; set; } = DateTime.Now;  
    
  
    }  
}  
